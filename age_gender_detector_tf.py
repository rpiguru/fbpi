import time
import cv2
import os
from pymongo import MongoClient
from settings import MONGO_DB, MONGO_COLLECTION_FACE
import threading

from utils.tf_detector.age_gender import detect_from_frame

mongo_client = MongoClient('localhost')
mongo_db = mongo_client[MONGO_DB]
col_face = mongo_db[MONGO_COLLECTION_FACE]


class AgeGenderDetector(threading.Thread):

    b_stop = threading.Event()

    def __init__(self):
        super(AgeGenderDetector, self).__init__()
        self.b_stop.clear()

    def run(self):
        while not self.b_stop.isSet():
            doc_list = list(col_face.find({"processed": False}))
            print(doc_list)
            if doc_list:
                for doc in doc_list:
                    print('Found an image - {}'.format(doc['image_path']))
                    frame = cv2.imread(doc['image_path'])
                    faces, ages, genders = detect_from_frame(frame)
                    print('Detected Faces - {}'.format(faces))
                    if faces:
                        col_face.update_one({"_id": doc['_id']},
                                            {"$set": {
                                                "processed": True,
                                                "age": ages[0],
                                                "gender": genders[0],
                                            }})
                        try:
                            os.remove(doc['image_path'])
                        except Exception as e:
                            print('Age/Gender: Failed to delete {} - {}'.format(doc['image_path'], e))
                    else:
                        print('Age/Gender: Face not found - {}'.format(doc['image_path']))
            time.sleep(5)

    def stop(self):
        self.b_stop.set()


if __name__ == '__main__':

    detector = AgeGenderDetector()

    try:
        detector.start()
    except KeyboardInterrupt:
        detector.stop()
