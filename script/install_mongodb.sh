#!/usr/bin/env bash

sudo apt-get install -y libssl-dev
cd /tmp
wget http://ftp.us.debian.org/debian/pool/main/o/openssl/libssl1.0.0_1.0.1t-1+deb8u7_armhf.deb
sudo dpkg -i libssl1.0.0_1.0.1t-1+deb8u7_armhf.deb

echo "Creating mongodb user..."
sudo adduser --ingroup nogroup --shell /etc/false --disabled-password --gecos "" --no-create-home mongodb

echo "Downloading mongodb compiled binaries... "
cd /tmp
wget http://andyfelong.com/downloads/core_mongodb_3_0_14.tar.gz
tar zxvf core_mongodb_3_0_14.tar.gz

sudo chown root:root mongo*
sudo chmod 755 mongo*
sudo strip mongo*
sudo cp -p mongo* /usr/bin
sudo rm mongo*

echo "Create log file directory with appropriate owner & permissions"
sudo mkdir /var/log/mongodb
sudo chown mongodb:nogroup /var/log/mongodb

echo "Create the DB data directory with convenient access perms"
sudo mkdir /var/lib/mongodb
sudo chown -R mongodb:root /var/lib/mongodb
sudo chmod 775 /var/lib/mongodb

echo "Create the mongodb.conf file in /etc"
sudo sh -c "cat > /etc/mongodb.conf << EOF
# /etc/mongodb.conf
# minimal config file (old style)
# Run mongod --help to see a list of options

bind_ip = 127.0.0.1
quiet = true
dbpath = /var/lib/mongodb
logpath = /var/log/mongodb/mongod.log
logappend = true
storageEngine = mmapv1
EOF"

echo "Create systemd/service entry"
sudo sh -c "cat > /lib/systemd/system/mongodb.service << EOF
[Unit]
Description=High-performance, schema-free document-oriented database
After=network.target

[Service]
User=mongodb
ExecStart=/usr/bin/mongod --quiet --config /etc/mongodb.conf

[Install]
WantedBy=multi-user.target

EOF"

sudo export LC_ALL=C

sudo systemctl enable mongodb

echo "Starting mongodb service..."
sudo service mongodb start
