#!/usr/bin/env bash
sudo apt-get install -y python-numpy python-scipy python-matplotlib python-skimage python-pandas python-pip
sudo pip install imutils

cd /tmp
wget https://github.com/lhelontra/tensorflow-on-arm/releases/download/v1.4.1/tensorflow-1.4.1-cp27-none-linux_armv7l.whl
sudo pip install tensorflow-1.4.1-cp27-none-linux_armv7l.whl
sudo pip uninstall mock
sudo pip install mock
