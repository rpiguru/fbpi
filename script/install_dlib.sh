#!/usr/bin/env bash

echo "========== Installing dlib on RPi =========="
cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

echo "== Temporarily Increasing SWAP file size =="
sudo sed -i -- 's/CONF_SWAPSIZE=100/CONF_SWAPSIZE=1536/g' /etc/dphys-swapfile
sudo /etc/init.d/dphys-swapfile stop
sudo /etc/init.d/dphys-swapfile start

cd /tmp
git clone https://github.com/davisking/dlib
cd dlib
sudo python setup.py install --yes USE_AVX_INSTRUCTIONS --compiler-flags "-O3"

sudo sed -i -- 's/CONF_SWAPSIZE=1536/CONF_SWAPSIZE=100/g' /etc/dphys-swapfile
sudo /etc/init.d/dphys-swapfile stop
sudo /etc/init.d/dphys-swapfile start
