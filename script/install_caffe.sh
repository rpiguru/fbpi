#!/usr/bin/env bash

echo "========== Installing caffe on RPi =========="
cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sudo apt-get install -y libprotobuf-dev libleveldb-dev libsnappy-dev libhdf5-serial-dev protobuf-compiler
sudo apt-get install -y libatlas-base-dev
sudo apt-get install -y --no-install-recommends libboost-all-dev
sudo apt-get install -y libgflags-dev libgoogle-glog-dev liblmdb-dev
sudo apt-get install -y python-dev python-pip
sudo apt-get install -y python-numpy python-scipy python-matplotlib python-skimage python-pandas

cd ~
wget https://github.com/BVLC/caffe/archive/rc5.zip
unzip rc5.zip
rm rc5.zip
cd caffe-rc5
cp Makefile.config.example Makefile.config
sed -i -- 's/# CPU_ONLY := 1/CPU_ONLY := 1/g' Makefile.config
sed -i -- 's/# OPENCV_VERSION := 3/OPENCV_VERSION := 3/g' Makefile.config
sed -i -- 's/# WITH_PYTHON_LAYER := 1/WITH_PYTHON_LAYER := 1/g' Makefile.config
sed -i -- 's/INCLUDE_DIRS := $(PYTHON_INCLUDE) \/usr\/local\/include/INCLUDE_DIRS := $(PYTHON_INCLUDE) \/usr\/local\/include \/usr\/include\/hdf5\/serial/g' Makefile.config
sed -i -- 's/LIBRARY_DIRS := $(PYTHON_LIB) \/usr\/local\/lib \/usr\/lib/LIBRARY_DIRS := $(PYTHON_LIB) \/usr\/local\/lib \/usr\/lib \/usr\/lib\/arm-linux-gnueabihf \/usr\/lib\/arm-linux-gnueabihf\/hdf5\/serial/g' Makefile.config
sed -i -- 's/NVCCFLAGS += -ccbin=$(CXX) -Xcompiler -fPIC $(COMMON_FLAGS)/NVCCFLAGS += -D_FORCE_INLINES -ccbin=$(CXX) -Xcompiler -fPIC $(COMMON_FLAGS)/g' Makefile
sed -i -- 's/LIBRARIES += glog gflags protobuf boost_system boost_filesystem m hdf5_hl hdf5/LIBRARIES += glog gflags protobuf leveldb snappy lmdb boost_system boost_filesystem hdf5_hl hdf5 m opencv_core opencv_highgui opencv_imgproc opencv_imgcodecs opencv_videoio/g' Makefile

echo "set(${CMAKE_CXX_FLAGS} \"-D_FORCE_INLINES ${CMAKE_CXX_FLAGS}\")" | tee -a CMakeLists.txt

find . -type f -exec sed -i -e 's^"hdf5.h"^"hdf5/serial/hdf5.h"^g' -e 's^"hdf5_hl.h"^"hdf5/serial/hdf5_hl.h"^g' '{}' \;
#cd /usr/lib/arm-linux-gnueabihf
#sudo ln -s libhdf5_serial.so.10.1.0 libhdf5.so
#sudo ln -s libhdf5_serial_hl.so.10.0.2 libhdf5_hl.so

cd /tmp
wget https://pypi.python.org/packages/03/98/1521e7274cfbcc678e9640e242a62cbcd18743f9c5761179da165c940eac/leveldb-0.20.tar.gz
tar zvxf leveldb-0.20.tar.gz
cd leveldb-0.20/leveldb/port
rm atomic_pointer.h
wget https://raw.githubusercontent.com/google/leveldb/c4c38f9c1f3bb405fe22a79c5611438f91208d09/port/atomic_pointer.h
cd /tmp/leveldb-0.20
sudo python setup.py install

cd /tmp
sudo pip install cython
wget https://pypi.python.org/packages/11/6b/32cee6f59e7a03ab7c60bb250caff63e2d20c33ebca47cf8c28f6a2d085c/h5py-2.7.0.tar.gz
tar zvxf h5py-2.7.0.tar.gz
cd h5py-2.7.0
sudo python setup.py install

sudo pip install ipython==5.0 networkx nose protobuf python-gflags pyyaml Pillow pymongo

cd ~/caffe-rc5
make all
make pycaffe
make distribute

echo "export PYTHONPATH=/home/pi/caffe-rc5/python:$PYTHONPATH" | tee -a ~/.bashrc
cp ${cur_dir}/caffe_io_modified.py /home/pi/caffe-rc5/python/caffe/io.py
