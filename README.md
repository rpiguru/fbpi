# Kiosk Pi

## Connect HC-SR04 UltraSonic Sensor to the RPi.
    
https://tutorials-raspberrypi.com/raspberry-pi-ultrasonic-sensor-hc-sr04/


## Installation

Execute installation script:

    bash install.sh

NOTE: RPi will reboot after this script.


## Update KioskPi

    bash update.sh

NOTE: RPi will reboot after this script.
