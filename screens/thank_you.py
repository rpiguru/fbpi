# -*- coding: iso8859-15 -*-

import os
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.video import Video
from screens.base import BaseScreen
from kivy.logger import Logger
from settings import WIDTH, HEIGHT
from utils.common import kill_process_by_name, is_rpi
import subprocess
from utils.video import get_video_size, get_video_length

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'thank_you.kv'))


VIDEO_FILE = 'assets/videos/nicole_thanks.mp4'


class ThankYouScreen(BaseScreen):

    video = None
    offset_x = 200

    def on_pre_enter(self, *args):
        super(ThankYouScreen, self).on_pre_enter(*args)
        Clock.schedule_once(self._show_title, 1)
        Clock.schedule_once(self.start_nicole, .1)

    def start_nicole(self, *args):
        x = self.offset_x
        x2 = WIDTH - x
        video_size = get_video_size(VIDEO_FILE)
        height = int((x2 - x) * video_size[1] / video_size[0])
        y = (HEIGHT - height) // 2
        y2 = y + height
        if is_rpi():
            cmd = "omxplayer -o local --aspect-mode letterbox --layer 20 --display 5 --win {},{},{},{} " \
                  "{}".format(x, y, x2, y2, VIDEO_FILE)
            Logger.debug('Thankyou: {}'.format(cmd))
            subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        else:
            self.video = Video(source='assets/videos/nicole_thanks.mp4', size_hint=(None, None), volume=0,
                               pos=(x, y), size=(x2 - x, y2 - y))
            self.add_widget(self.video)
            self.video.state = "play"
        Clock.schedule_once(lambda dt: self._on_nicole_finished(), get_video_length(VIDEO_FILE) + 1.5)

    def on_pre_leave(self, *args):
        kill_process_by_name('omxplayer.bin')
        super(ThankYouScreen, self).on_pre_leave(*args)

    def _on_nicole_finished(self):
        self.switch_screen('home')

    def _show_title(self, *args):
        self.ids.title.opacity = 1
