# -*- coding: iso8859-15 -*-

import os
import threading
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.core.window import Window
from screens.base import BaseScreen
from settings import PREVIEW_TIMEOUT
from utils.e_mail import is_valid_email, send_email
from utils.store_selectors import CurEvent
from kivy.logger import Logger

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'email.kv'))


class EmailScreen(BaseScreen):
    _allow_focus = True
    _cnt = PREVIEW_TIMEOUT

    def __init__(self, **kwargs):
        super(EmailScreen, self).__init__(**kwargs)
        self.ids.txt_email.bind(focus=self.on_email_focus)
        self.ids.txt_email.bind(text=self.on_email_text)

    def on_enter(self, *args):
        self._cnt = PREVIEW_TIMEOUT
        Clock.schedule_interval(self.count_down, 1)
        self._allow_focus = True
        self.on_email_focus()
        super(EmailScreen, self).on_enter(*args)

    def on_email_focus(self, *args):
        if not self.ids.txt_email.focus and self._allow_focus:
            self.ids.txt_email.focus = True

    def on_btn_send(self):
        email = self.ids.txt_email.text
        if is_valid_email(email):
            if CurEvent().get().get('Email'):
                threading.Thread(target=send_email, args=(email, )).start()
            else:
                Logger.warning('Email: XML file does not contain `Email` field.')
            self.return_to_home()

    def on_btn_cancel(self):
        self.return_to_home()

    def on_leave(self, *args):
        self._allow_focus = False
        self._cnt = -1
        Window.release_all_keyboards()
        super(EmailScreen, self).on_leave(*args)

    def on_touch_down(self, touch):
        """
        Update timeout value when user touches screen
        :param touch:
        :return:
        """
        super(EmailScreen, self).on_touch_down(touch)
        self._cnt = PREVIEW_TIMEOUT

    def on_email_text(self, *args):
        """
        Update the timeout value when the text changes
        :param args:
        :return:
        """
        self._cnt = PREVIEW_TIMEOUT

    def count_down(self, *args):
        self._cnt -= 1
        if self._cnt == 0:
            self.return_to_home()
            return False        # Cancel this
