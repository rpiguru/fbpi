# -*- coding: iso8859-15 -*-

import random
import os
from kivy.animation import Animation
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import NumericProperty
from kivy.uix.video import Video
from kivy.uix.widget import Widget
from kivy.uix.image import Image
from functools import partial
from kivymd.snackbar import Snackbar
from screens.base import CameraBaseScreen
from settings import *
from utils.common import kill_process_by_name, is_rpi
from utils.common import get_image_name
from utils.image import create_thumb_image, get_template_size
from utils.store_selectors import SelectedImages, CurrentEventImages, CurEvent, get_current_event_name
from utils.video import play_video_on_widget
from widgets.image import DraggableImage, KioskActionImage
from utils.usbrelay import turn_relay

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'select.kv'))

SELECT_SCREEN_MAX_PICTURES = SELECT_SCREEN_PICTURES_COLS * SELECT_SCREEN_PICTURES_ROWS


class SelectScreen(CameraBaseScreen):

    current_photo_number = 0
    count_down_time = NumericProperty(SELECT_TIMEOUT)
    print_photo = False
    selected_photo_widgets = []
    photo_widgets = []
    countdown_timer = None
    overlay_img = None

    def __init__(self, **kwargs):
        super(SelectScreen, self).__init__(**kwargs)
        for i in range(SELECT_SCREEN_MAX_PICTURES):
            self.ids.pictures_layout.add_widget(Widget())

    def on_enter(self, *args):
        super(SelectScreen, self).on_enter(*args)
        # Resize template image
        w, h = get_template_size(get_current_event_name())
        if WIDTH / HEIGHT < w / h:
            self.ids.box_collage.width = WIDTH / 2 - 25
            self.ids.box_collage.height = self.ids.box_collage.width * h / w
        else:
            self.ids.box_collage.height = HEIGHT / 2 - 25
            self.ids.box_collage.width = self.ids.box_collage.height * w / h

        self.show_preview_on_widget(self.ids.box_preview)
        self.current_photo_number = 0
        self.count_down_time = SELECT_TIMEOUT
        self.selected_photo_widgets = []
        self.photo_widgets = []
        self.countdown_timer = None
        Clock.schedule_once(self.start_nicole, .1)
        self.is_recording = False

    def start_nicole(self, *args):
        """
        Play Nicole at the top-right side.
        :param args:
        :return:
        """
        if is_rpi():
            play_video_on_widget(widget=self.ids.top_right_box, video_src=self.video_file)
            turn_relay('on')
        else:
            video_widget = Video(source=self.video_file, volume=0, pos_hint={'x': 0, 'y': 0}, allow_stretch=True)
            self.ids.top_right_box.add_widget(video_widget)
            video_widget.state = 'play'
            self.ids.box_collage.opacity = 1

        Clock.schedule_once(self.start_process, NICOLE_PHOTO_START)

    def start_process(self, *args):
        """
        Start the preview display and show it for two seconds before actually taking the photo
        :param args:
        :return:
        """
        self.is_recording = True
        Clock.schedule_once(self.take_photo, .5)

    def take_photo(self, *args):
        """
        Actually take the photo. Display it in the preview area for 1 second
        :return:
        """
        if not is_rpi():
            img_path = 'assets/images/sample/{}.jpg'.format(self.current_photo_number + 1)
        else:
            img_path = get_image_name(event_name=get_current_event_name(), base_path=IMAGE_RECORDED_FILES)
            self.take_picture(img_path)
        Clock.schedule_once(partial(self._load_image, img_path))

    def _load_image(self, img_path, *args):
        img = DraggableImage(dragable=False, source=img_path, size_hint=(None, None), size=self.ids.box_preview.size,
                             allow_stretch=True, pos=self.ids.box_preview.pos, bound_zone_objects=[self, ],
                             number=self.current_photo_number, drop_func=self.on_drop_finish)
        self.photo_widgets.append(img)
        self.add_widget(img)
        Clock.schedule_once(partial(self._start_movement, img), .2)

    def _start_movement(self, img, *args):
        dest_widget = self.ids.pictures_layout.children[SELECT_SCREEN_MAX_PICTURES - self.current_photo_number - 1]
        img.original_pos = dest_widget.pos
        anim = Animation(size=dest_widget.size, pos=dest_widget.pos, t='out_back')
        anim.bind(on_complete=self.on_move_finished)
        anim.start(img)

    def on_move_finished(self, *args):
        """
        Callback when an image is moved to its position
        :param args:
        :return:
        """
        self.current_photo_number += 1
        if self.current_photo_number < SELECT_SCREEN_MAX_PICTURES:
            Clock.schedule_once(self.start_process)
        else:
            self.is_recording = False
            Clock.schedule_once(self.on_finished_capture, 2.5)

    def on_finished_capture(self, *args):
        """
        Add template background image
        :param args:
        :return:
        """
        turn_relay('off')
        self.stop_preview()
        kill_process_by_name('omxplayer.bin')
        bg_img = Image(source='events/{}/default.jpg'.format(get_current_event_name()), size_hint=(None, None),
                       size=self.ids.top_right_box.size,
                       allow_stretch=True, pos=self.ids.top_right_box.pos, keep_ratio=False)
        self.add_widget(bg_img)
        Clock.schedule_once(partial(self.on_template_image_added, bg_img))

    def on_template_image_added(self, bg_img, *args):
        """
        Add individual image blocks
        :return:
        """
        for p in CurEvent().get()['Print']['photo']:
            widget = KioskActionImage(size_hint=(None, None), angle=int(p['rotation']), allow_stretch=True)
            ratio_x = bg_img.width / bg_img.texture_size[0]
            ratio_y = bg_img.height / bg_img.texture_size[1]
            widget.width = int(p['width']) * ratio_x
            widget.height = int(p['height']) * ratio_y
            widget.pos[0] = self.ids.top_right_box.pos[0] + int(p['startx']) * ratio_x
            widget.pos[1] = self.ids.top_right_box.pos[1] + \
                            int(bg_img.texture_size[1] - int(p['starty'])) * ratio_y - widget.height
            self.add_widget(widget)
            self.selected_photo_widgets.append(widget)

        # Enable Drag & Drop for each images
        for img in self.photo_widgets:
            img.dragable = True
            img.droppable_zone_objects = self.selected_photo_widgets
        Clock.schedule_once(self.add_overlay_image)
        Clock.schedule_once(self.fill_random_images)

    def add_overlay_image(self, *args):
        if self.overlay_img:
            self.get_root_window().remove_widget(self.overlay_img)
        self.overlay_img = Image(source='events/{}/overlay.png'.format(get_current_event_name()),
                                 size=self.ids.top_right_box.size, size_hint=(None, None),
                                 pos=self.ids.top_right_box.pos)
        self.get_root_window().add_widget(self.overlay_img)

    def fill_random_images(self, *args):
        """Fill with random images
        """
        for i, j in enumerate(random.sample(range(0, SELECT_SCREEN_MAX_PICTURES-1), len(self.selected_photo_widgets))):
            self.selected_photo_widgets[i].original_source = self.photo_widgets[j].source
            self.selected_photo_widgets[i].src_num = self.photo_widgets[j].number
            self.photo_widgets[j].opacity = 0
            self.photo_widgets[j].dragable = False

        self.ids.box_collage.opacity = 1
        Clock.schedule_once(self.show_count_down_timer, 1)

    def show_count_down_timer(self, *args):
        """Show Count down Timer
        """
        self.ids.box_counter.opacity = 1
        self.ids.btn_print.disabled = False
        self.ids.btn_print.opacity = 1
        self.countdown_timer = Clock.schedule_interval(self.selecting_countdown, 1)

    def on_drop_finish(self, *args):
        """
        Callback when drop was done successfully
        :param args: [D&D object, Destination object]
        :return:
        """
        dnd_img = args[0][0]
        dest_obj = args[0][1]
        dest_obj.dragable = True
        if dest_obj.src_num >= 0:        # Restore the old image
            self.photo_widgets[dest_obj.src_num].opacity = 1
            self.photo_widgets[dest_obj.src_num].dragable = True
        dnd_img.pos = (dnd_img.old_x, dnd_img.old_y)       # Return back to the original position...
        dnd_img.opacity = 0
        dnd_img.dragable = False
        dest_obj.original_source = dnd_img.source
        dest_obj.src_num = dnd_img.number
        Clock.schedule_once(self.add_overlay_image, .1)

    def selecting_countdown(self, *args):
        if self.count_down_time == 0:
            self._cancel_countdown()
            self.save_selected_photos()
            self.switch_screen('home')
        else:
            self.count_down_time -= 1

    def on_touch_down(self, touch):
        super(SelectScreen, self).on_touch_down(touch)
        self.count_down_time = SELECT_TIMEOUT

    def save_selected_photos(self):
        """
        Compose a result image and save to disk
        :return:
        """
        img_paths = [img.original_source for img in self.selected_photo_widgets if img.src_num >= 0]
        old_images = SelectedImages().get()
        SelectedImages().set(old_images + img_paths)
        CurrentEventImages().set(img_paths)
        for path in img_paths:
            if is_rpi():
                create_thumb_image(path)

    def on_btn_print(self):
        if any([img.src_num < 0 for img in self.selected_photo_widgets]):
            Snackbar(text='Please fill all images', background_color=(.8, 0, .3, .5)).show()
            return
        self.save_selected_photos()
        self.switch_screen('thank_you')

    def _cancel_countdown(self):
        if self.countdown_timer:
            self.countdown_timer.cancel()
            self.countdown_timer = None

    def on_pre_leave(self, *args):
        self.clear_images()
        self._cancel_countdown()
        super(SelectScreen, self).on_pre_leave(*args)

    def clear_images(self, *args):
        if self.overlay_img:
            self.get_root_window().remove_widget(self.overlay_img)
        selected_images = SelectedImages().get()
        for img in self.photo_widgets:
            if is_rpi():
                if img.source not in selected_images:
                    os.remove(img.source)
            try:
                img.deparent()
            except AttributeError:
                pass
