# -*- coding: iso8859-15 -*-

from kivy.uix.screenmanager import ScreenManager
from screens.calibration import CalibrationScreen
from screens.email import EmailScreen
from screens.error import ErrorScreen
from screens.event import EventsScreen
from screens.action_video import ActionVideoScreen
from screens.network import NetworkScreen
from screens.select import SelectScreen
from screens.home import HomeScreen
from screens.thank_you import ThankYouScreen


screens = {
    'home': HomeScreen,
    'select': SelectScreen,
    'thank_you': ThankYouScreen,
    'email': EmailScreen,
    'events': EventsScreen,
    'network': NetworkScreen,
    'action_video': ActionVideoScreen,
    'error': ErrorScreen,
    'calibration': CalibrationScreen,
}

sm = ScreenManager()
