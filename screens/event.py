# -*- coding: iso8859-15 -*-

import datetime
import os
import time
from functools import partial
from kivy.clock import Clock
from kivy.lang import Builder
from screens.base import BaseScreen
from utils.common import number_to_ordinal, get_serial
from utils.config import get_config_data
from utils.store_selectors import PreviewMode, CurEvent
from widgets.event_item import EventItem
from kivy.logger import Logger

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'event.kv'))


class EventsScreen(BaseScreen):

    def on_enter(self, *args):
        super(EventsScreen, self).on_enter(*args)
        self.show_datetime()
        Clock.schedule_interval(self.show_datetime, 1)

        self.ids.event_list.clear_widgets()
        current_time = time.time()
        event_data = get_config_data()
        events = event_data['eventconfig']['event']
        if type(events) != list:
            events = [events]
        for event in events:
            if event['@id'] == 'default':
                continue
            if int(event['StartTime']) < current_time:
                Logger.warning('Event: {} was already ended.'.format(event['@id']))
                continue
            start_time = datetime.datetime.fromtimestamp(int(event['StartTime'])).strftime('%Y-%m-%d %I:%M:%S %p')
            end_time = datetime.datetime.fromtimestamp(int(event['EndTime'])).strftime('%Y-%m-%d %I:%M:%S %p')
            event_item = EventItem(text=event['@id'], secondary_text='{} - {}'.format(start_time, end_time))
            event_item.bind(on_release=partial(self.on_event_press, event))
            self.ids.event_list.add_widget(event_item)

        self.ids.hardware_serial_label.text = get_serial()

    def on_event_press(self, event, instance):
        PreviewMode().set(True)
        CurEvent().set(event)
        Logger.info('Kiosk: Starting Preview mode of {}'.format(event['@id']))
        self.switch_screen('select')

    def on_btn_cancel(self):
        self.switch_screen('home', 'right')

    def show_datetime(self, *args):
        cur_time = datetime.datetime.now()
        self.ids.txt_am_pm.text = cur_time.strftime("%p")
        self.ids.txt_time.text = cur_time.strftime("%I:%M:%S")
        self.ids.txt_day.text = cur_time.strftime("%A")
        self.ids.txt_date.text = cur_time.strftime("{} of %B".format(number_to_ordinal(cur_time.day)))

    def on_btn_calibrate(self):
        self.switch_screen('calibration', 'left')
