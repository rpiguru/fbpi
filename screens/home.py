# -*- coding: iso8859-15 -*-

import os
import subprocess
import threading
import time
import datetime
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from pymongo import MongoClient
from screens.base import BaseScreen
from settings import MAIN_SCREEN_COLS, MAIN_SCREEN_ROWS, PIN_TRIG, PIN_ECHO, UPDATE_EVENT, CHECK_DISTANCE, \
    DISTANCE_THRESHOLD, MONGO_DB, MONGO_COLLECTION_FACE
from utils.common import kill_process_by_name, is_rpi, get_serial
from utils.net import check_network_connection
from kivy.logger import Logger
from utils.video import play_video_on_widget, get_video_length

if is_rpi():
    from utils.hc_sr04 import DistanceReader


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'home.kv'))


MAIN_SCREEN_MAX_PHOTOS = MAIN_SCREEN_COLS * MAIN_SCREEN_ROWS
mongo_client = MongoClient('localhost')
mongo_db = mongo_client[MONGO_DB]
col_face = mongo_db[MONGO_COLLECTION_FACE]


ANIMATION_PATH = 'assets/videos/nicole_home_animation.mp4'


class HomeScreen(BaseScreen):

    _tr_network = None
    _stop = threading.Event()
    is_connected = False
    clk_event = None

    def __init__(self, **kwargs):
        super(HomeScreen, self).__init__(**kwargs)
        self._tr_network = threading.Thread(target=self._check_network)

    def on_pre_enter(self, *args):
        self._stop.clear()
        try:
            self._tr_network.join()
        except RuntimeError:
            pass
        self._tr_network = threading.Thread(target=self._check_network)
        self._tr_network.start()
        if is_rpi():
            self.ids.nicole.opacity = 0
        if is_rpi() and CHECK_DISTANCE:
            threading.Thread(target=self._check_distance).start()
        super(HomeScreen, self).on_pre_enter(*args)

    def _check_network(self):
        while not self._stop.isSet() and UPDATE_EVENT:
            network_connected = check_network_connection()
            # Update status only when connection state is changed.
            if network_connected != self.is_connected:
                self.is_connected = network_connected
                Clock.schedule_once(lambda dt: self.update_network_status())
            time.sleep(3)

    @mainthread
    def update_network_status(self):
        self.ids.network_button.source = 'assets/images/globe.png' \
            if self.is_connected else 'assets/images/globe-noconnection.png'

    def on_pre_leave(self, *args):
        kill_process_by_name('omxplayer.bin')
        self._stop.set()
        super(HomeScreen, self).on_pre_leave(*args)

    def on_btn_info(self):
        self.switch_screen('events', 'left')

    def on_btn_network(self):
        self.switch_screen('network', 'right')

    def _check_distance(self):
        reader = DistanceReader(TRIG=PIN_TRIG, ECHO=PIN_ECHO)
        played_time = 0
        video_length = get_video_length(ANIMATION_PATH)
        while not self._stop.isSet():
            distance = reader.read_distance()
            if 0 < distance < DISTANCE_THRESHOLD:
                if time.time() - played_time > video_length:
                    Logger.info('Kiosk: Person(Object) detected ({}cm). Playing `hello.mp3`'.format(distance))
                    cmd = 'omxplayer -o local assets/sounds/hello.mp3'
                    subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    play_video_on_widget(widget=self.ids.nicole, video_src=ANIMATION_PATH)
                    played_time = time.time()

            time.sleep(.5)

    def on_btn_emoji(self, _type='happy'):
        Logger.info('Kiosk: {} :: {} emoji is pressed'.format(datetime.datetime.now(), _type))
        self._stop.set()
        file_name = 'events/default/{}-{}_{}.jpg'.format(get_serial(), time.time().__int__(), _type)
        if is_rpi():
            self.app.camera.take_picture(file_name)
        col_face.insert_one({
            'processed': False,
            'image_path': file_name,
            'timestamp': datetime.datetime.now(),
            'feeling': _type,
        })
        self.switch_screen('thank_you')
