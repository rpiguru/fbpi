# -*- coding: iso8859-15 -*-
import os
from kivy.lang import Builder
from screens.base import BaseScreen

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'error.kv'))


class ErrorScreen(BaseScreen):

    def on_enter(self, *args):
        error_msg = self.app.get_exception()
        # self.ids.error_msg.text = str(error_msg)
        super(ErrorScreen, self).on_enter(*args)

    def on_btn_back(self):
        self.switch_screen('home')
