import datetime
from pymongo import MongoClient

from settings import MONGO_DB, MONGO_COLLECTION_FACE

mongo_client = MongoClient('localhost')
mongo_db = mongo_client[MONGO_DB]
col_face = mongo_db[MONGO_COLLECTION_FACE]

file_name = '/tmp/sample.jpg'

col_face.insert_one({
    'processed': False,
    'image_path': file_name,
    'timestamp': datetime.datetime.now(),
    'feeling': 'happy',
})
