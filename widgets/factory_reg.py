# -*- coding: iso8859-15 -*-

from kivy.factory import Factory

# Custom widgets here
from widgets.button import ImageButton
from widgets.spinner import KioskSpinner
from widgets.checkbox import LabeledCheckbox
from widgets.label import *
from widgets.loading_indicator import LoadingIndicator


Factory.register('LabeledCheckbox', cls=LabeledCheckbox)
Factory.register('H0', cls=H0)
Factory.register('H1', cls=H1)
Factory.register('H2', cls=H2)
Factory.register('H3', cls=H3)
Factory.register('H4', cls=H4)
Factory.register('H5', cls=H4)
Factory.register('H6', cls=H6)
Factory.register('P', cls=P)
Factory.register('ScrollableLabel', cls=ScrollableLabel)
Factory.register('LoadingIndicator', cls=LoadingIndicator)
Factory.register('KioskSpinner', cls=KioskSpinner)
Factory.register('ImageButton', cls=ImageButton)
