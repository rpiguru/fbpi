# -*- coding: iso8859-15 -*-

import os
import time
from kivy.lang import Builder
from kivy.properties import NumericProperty, StringProperty
from kivy.uix.image import Image
from utils.image import crop_image
from widgets.DragNDropWidget import DragNDropWidget

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'image.kv'))


class KioskImage(Image):
    angle = NumericProperty()


class DraggableImage(DragNDropWidget, Image):
    keep_ratio = False
    drag_size_hint = [.5, .5]
    number = NumericProperty(-1)


class KioskActionImage(KioskImage):
    src_num = NumericProperty(-1)
    original_source = StringProperty('')

    def __init__(self, **kwargs):
        super(KioskActionImage, self).__init__(**kwargs)
        self.bind(src_num=self._update_texture)
        self.bind(original_source=self._update_texture)
        self._update_texture()

    def _update_texture(self, *args):
        if self.src_num >= 0 and self.original_source != '':
            resized_img = crop_image(img=None, width=int(self.size[0]), height=int(self.size[1]),
                                     path=self.original_source)
            tmp_path = '/tmp/{}.jpg'.format(time.time())
            resized_img.save(tmp_path)
            self.source = tmp_path
        else:
            self.source = 'assets/images/dashed_rect.png'
