# -*- coding: iso8859-15 -*-

import os
from abc import abstractmethod
from kivy.animation import Animation
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import NumericProperty, BooleanProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.widget import Widget

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'base.kv'))

animation = True


class AnimatedWidget(Widget):
    animation_offset = NumericProperty(None, allownone=True)
    scale_factor = NumericProperty(1)
    animate_opacity = BooleanProperty(True)
    animate_scale = BooleanProperty(True)
    _start_scale_factor = NumericProperty(.95)
    _original_opacity = NumericProperty(1)
    _appear_animation_duration = NumericProperty(.4)

    def set_visible(self, is_visible):
        self.opacity = int(is_visible)
        self.disabled = not is_visible or self.disabled

    def reset_widget(self):
        """
        Set widget initial state.
        Call this in on_pre_enter screen method.
        :return:
        """
        if self.animation_offset:
            self._original_opacity = self.opacity
            if self.animate_scale:
                self.scale_factor = self._start_scale_factor

            if self.animate_opacity:
                self.opacity = 0

    def hide(self):
        """
        Hide widget
        :return:
        """
        if self.animation_offset and animation:
            self._hide()

    def _hide(self):
        """
        Hide widget animation
        :return:
        """
        hide_animation = Animation(scale_factor=self._start_scale_factor,
                                   opacity=0,
                                   duration=self._appear_animation_duration,
                                   t='in_out_quad')
        hide_animation.start(self)

    def appear(self):
        """
        Start widget appear animation.
        Call this in on_enter screen method.
        :return:
        """
        if self.animation_offset:
            if animation:
                _self = self
                appear_animation = Animation(scale_factor=1,
                                             opacity=self._original_opacity,
                                             duration=self._appear_animation_duration,
                                             t='in_out_quad')
                Clock.schedule_once(lambda dt: appear_animation.start(_self),
                                    self.animation_offset)
            else:
                self.opacity = 1


class DefaultInput(object):
    """
    If you want to create any input widget you should inherit this class.
    For example::

        MyAwesomeInput(TextInput, DefaultInput):

    Also make sure your widget doesn't contain any other widgets with the key property
    otherwise the screens.base.screen.BaseScreen will collect them too.
    """
    key = StringProperty(None)
    """Key used to collect data from screen
    """

    required = BooleanProperty(True)

    error_label_id = StringProperty('error')
    """Label id to display error messages
    """

    child_input = StringProperty(None)
    """Id of the child input widget
    If you want to create an input widget like::
        BoxLayout:
            Label:
                text: 'Input Name'

            TextInput:
                id: input

    just set this property to TextInout id.
    Works if there is only one child input
    """

    @property
    def _widget(self):
        if self.child_input:
            # try:
            return self.ids[self.child_input]
            # except
        else:
            return self

    def show_error(self, message):
        self.mark_as_error()
        self.show_error_message(message)

    def clear_errors(self):
        self.mark_as_normal()
        self.hide_error_message()

    def _is_empty_value(self, val):
        return val is None or val == ''

    def _validate(self):
        errors = []
        value = self._widget._get_value()
        if isinstance(value, str):
            value = value.lstrip()
        if self._is_empty_value(value) and self.required and not self.disabled:
            errors.append('This field may not be blank')

        custom_errors = self.custom_error_check()
        if custom_errors:
            errors += custom_errors

        return errors

    def validate(self):
        """
        If there is any errors return a dict of field key and errors list
        similar to server response
        :return:
        """
        errors = self._validate()
        return {self.key: errors} if errors else {}

    def _get_error_message_label(self):
        if hasattr(self.ids, self.error_label_id):
            return getattr(self.ids, self.error_label_id)

        return None

    def show_error_message(self, message):
        """
        Override to put the error message to specific label on your widget
        :param message: str
        :return:
        """
        widget = self._get_error_message_label()
        if widget:
            widget.text = message
            widget.height = 15

    def hide_error_message(self):
        widget = self._get_error_message_label()
        if widget:
            widget.text = ''
            widget.height = 0.1

    def get_value(self):
        """
        Returns a dict of key and value
        :return: dict()
        """
        return {self.key: self._widget._get_value()}

    @abstractmethod
    def custom_error_check(self):
        """
        Write your custom error check here
        :return: list
        """
        if self.child_input:
            return self._widget.custom_error_check()

        return None

    @abstractmethod
    def _get_value(self):
        """
        Returns value
        :return: any
        """
        if self.child_input:
            return self._widget._get_value()

    @abstractmethod
    def set_value(self, value):
        """
        Set the widget value
        :param value:
        :return:
        """
        if self.child_input:
            self._widget.set_value(value)

    @abstractmethod
    def mark_as_error(self):
        """
        Highlight your widget
        :return:
        """
        if self.child_input:
            self._widget.mark_as_error()

    @abstractmethod
    def mark_as_normal(self):
        """
        Remove highlight
        :return:
        """
        if self.child_input:
            self._widget.mark_as_normal()


class LabeledInputBase(BoxLayout, DefaultInput, AnimatedWidget):
    _optional_text = StringProperty()
    """Hack for localization
    """

    def custom_error_check(self):
        pass

    def _get_value(self):
        pass

    def mark_as_error(self):
        pass

    def mark_as_normal(self):
        pass

    text = StringProperty()

    child_input = 'input'

    def __init__(self, **kwargs):
        super(LabeledInputBase, self).__init__(**kwargs)
        self.bind(text=self._update_text)
        self.bind(required=self._update_text)
        self.bind(_optional_text=self._update_text)

    def _update_text(self, *args, **kwargs):
        if not self.required:
            self.ids.text.text = '{} [color=78909c]({})'.format(self.text, self._optional_text)
        else:
            self.ids.text.text = self.text

    def set_value(self, value):
        self.ids.input.set_value(value)

    def on_touch_down(self, touch):
        if self.collide_point(touch.x, touch.y):
            self.clear_errors()

        super(LabeledInputBase, self).on_touch_down(touch)


class ClickableWidget(Widget):

    def __init__(self, **kwargs):
        self.register_event_type('on_press')
        super(ClickableWidget, self).__init__(**kwargs)

    def on_touch_down(self, touch):
        super(ClickableWidget, self).on_touch_down(touch)
        if self.collide_point(*touch.pos):
            self.dispatch('on_press')

    def on_press(self):
        pass
