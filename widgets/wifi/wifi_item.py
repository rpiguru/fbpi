# -*- coding: iso8859-15 -*-

import os
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.clock import Clock

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'wifi_item.kv'))


class WiFiItem(BoxLayout):

    text = StringProperty('')
    signal_percentage = NumericProperty(0)
    icon_strength = NumericProperty(0)
    connected = BooleanProperty(False)
    saved = BooleanProperty(False)
    secondary_text = StringProperty()

    def __init__(self, **kwargs):
        super(WiFiItem, self).__init__(**kwargs)
        self.register_event_type('on_release')
        self.register_event_type('on_forget')
        img_index = int(self.signal_percentage / 20)
        if img_index == 5:
            img_index = 4
        self.ids.img_wifi.source = 'assets/images/wifi/{}.png'.format(img_index)
        Clock.schedule_once(self.calibrate)
        if not self.saved:
            self.remove_widget(self.ids.btn_forget)

    def calibrate(self, *args):
        if self.ids.lb_text.height < 30:
            self.ids.box_text.padding = [0, 10, 0, 0]

    def on_release(self):
        pass

    def on_forget(self, *args):
        pass

    def on_btn_forget(self):
        self.dispatch('on_forget')

    def on_touch_down(self, touch):
        super(WiFiItem, self).on_touch_down(touch)
        if self.collide_point(*touch.pos):
            if not self.saved or not self.ids.btn_forget.collide_point(*touch.pos):
                self.dispatch('on_release')


if __name__ == '__main__':
    from kivy.base import runTouchApp
    runTouchApp(WiFiItem())
