# -*- coding: UTF-8 -*-
from kivy.animation import Animation
from kivy.uix.widget import Widget
from kivy.properties import ListProperty, NumericProperty, BooleanProperty, ObjectProperty
import time


class DragNDropWidget(Widget):

    droppable_zone_objects = ListProperty()

    bound_zone_objects = ListProperty()

    drag_opacity = NumericProperty(1.0)

    drop_func = ObjectProperty(None)

    invalid_drop_func = ObjectProperty(None)

    press_func = ObjectProperty(None)

    drop_args = ListProperty()

    remove_on_drag = BooleanProperty(True)
    """If True then the widget will disappear from its parent on drag, else the widget will just 
    get copied for dragging.
    """

    dragable = BooleanProperty(True)

    drag_size_hint = ListProperty([1, 1])

    original_pos = ListProperty()

    start_time = None
    min_x = 0
    min_y = 0
    max_x = 0
    max_y = 0
    short_touch = False

    def __init__(self, **kw):
        super(DragNDropWidget, self).__init__(**kw)

        self.register_event_type("on_drag_start")
        self.register_event_type("on_being_dragged")
        self.register_event_type("on_drag_finish")

        self._dragged = False
        self.old_x = 0
        self.old_y = 0

    def set_remove_on_drag(self, value):
        """
        This function sets the property that determines whether the dragged widget is just copied from its parent
        or taken from its parent.
        @param value: either True or False.
            If True then the widget will disappear from its parent on drag, else the widget will just
                get copied for dragging.
        """
        self.remove_on_drag = value

    def set_bound_axis_positions(self):

        for obj in self.bound_zone_objects:
            try:
                if self.max_y < obj.y + obj.size[1] - self.size[1] / 2:
                    self.max_y = obj.y + obj.size[1] - self.size[1] / 2
            except AttributeError:
                self.max_y = obj.y + obj.size[1] - self.size[1] / 2
            try:
                if self.max_x < obj.x + obj.size[0] - self.size[0] / 2:
                    self.max_x = obj.x + obj.size[0] - self.size[0] / 2
            except AttributeError:
                self.max_x = obj.x + obj.size[0] - self.size[0] / 2
            try:
                if self.min_y > obj.y:
                    self.min_y = obj.y
            except AttributeError:
                self.min_y = obj.y
            try:
                if self.min_x > obj.x:
                    self.min_x = obj.x
            except AttributeError:
                self.min_x = obj.x

    def on_touch_down(self, touch):
        if self.collide_point(touch.x, touch.y) and self.dragable:
            # detect if the touch is short - has time and end (if not dispatch drag)
            if abs(touch.time_end - touch.time_start) > 0.2:
                self.dispatch("on_drag_start")

    def on_touch_up(self, touch):
        if self.dragable and self._dragged:
            self.short_touch = True
            self.dispatch("on_drag_finish")
            self.short_touch = False

    def on_touch_move(self, touch):
        if self._dragged and self.dragable:
            x = touch.x - self.width / 2
            y = touch.y - self.height / 2
            x = max(self.min_x, x)
            x = min(self.max_x, x)
            y = max(self.min_y, y)
            y = min(self.max_y, y)
            self.pos = (x, y)

    def on_drag_start(self):
        self.start_time = time.time()
        self.opacity = self.drag_opacity
        self.set_bound_axis_positions()
        self.old_x = self.pos[0]
        self.old_y = self.pos[1]

        self._dragged = True

        # create copy of object to drag
        self.reparent(self)
        self.width *= self.drag_size_hint[0]
        self.height *= self.drag_size_hint[1]

        if self.press_func:
            self.press_func(self)

    def on_drag_finish(self):
        """
        occurs when user finishes drag & drop
        """
        if self.start_time:
            if time.time() - self.start_time > 0.2:
                if self._dragged and self.dragable:
                    self.opacity = 1.0
                    dropped_ok = False
                    dest_obj = None
                    for obj in self.droppable_zone_objects:
                        if abs(obj.center_x - self.center_x) + abs(obj.center_y - self.center_y) < 200:
                            dest_obj = obj
                            dropped_ok = True
                            break

                    if dropped_ok:
                        if self.drop_func:
                            self.drop_func([self, dest_obj])
                    else:
                        anim = Animation(pos=self.original_pos if self.original_pos else [self.old_x, self.old_y],
                                         duration=0.3, t="in_quad")
                        anim.bind(on_complete=self.on_return_anim_finished)
                        if self.invalid_drop_func:
                            self.invalid_drop_func(self)
                        anim.start(self)
                        self.dragable = False
                    self._dragged = False
            else:
                anim = Animation(pos=self.original_pos if self.original_pos else [self.old_x, self.old_y],
                                 duration=0.3, t="in_quad")
                if self.invalid_drop_func:
                    self.invalid_drop_func(self)
                anim.bind(on_complete=self.on_return_anim_finished)
                self._dragged = False
                anim.start(self)
            self.width /= self.drag_size_hint[0]
            self.height /= self.drag_size_hint[1]
        self.start_time = None

    def on_return_anim_finished(self, *args):
        if self.remove_on_drag:
            self.reborn()
        else:
            self.deparent()
        self.dragable = True

    def deparent(self):
        self.get_root_window().remove_widget(self)

    def on_being_dragged(self):
        print("being dragged")

    def reborn(self, widget='dumb', anim='dumb2'):
        pass

    def reparent(self, widget):
        parent = widget.parent
        orig_size = widget.size
        if parent:
            parent.remove_widget(widget)
            parent.get_root_window().add_widget(widget)
            widget.size_hint = (None, None)
            widget.size = orig_size
