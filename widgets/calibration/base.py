# -*- coding: iso8859-15 -*-

from abc import abstractmethod

from kivy.clock import Clock
from kivy.properties import StringProperty
from kivy.uix.boxlayout import BoxLayout


class CalibrationWidgetBase(BoxLayout):

    key = StringProperty()
    title = StringProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.register_event_type('on_changed')
        Clock.schedule_once(lambda dt: self._bind_child())

    @abstractmethod
    def _bind_child(self):
        pass

    @abstractmethod
    def set_value(self, val):
        pass

    @abstractmethod
    def get_value(self):
        pass

    def on_changed(self):
        pass
