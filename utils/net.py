# -*- coding: iso8859-15 -*-

"""
    Utility module related to net
"""
import http.client
import os
import random
import shutil
import socket
import subprocess
import json
import urllib.request
import time
import psutil
import requests
import wifi
import netifaces
from requests import get
from kivy.config import _is_rpi
from settings import URL_CONNECTIVITY
from utils.common import get_serial
from kivy.logger import Logger

try:
    import fcntl
except ImportError:
    pass

measure_time = time.time()
traffic_data = None


def check_internet_connection():
    """
    Check internet connection
        It will be faster to just make a HEAD request so no HTML will be fetched.
        Also I am sure google would like it better this way :)
    """
    _url = 'www.google.com'
    conn = http.client.HTTPConnection(_url, timeout=5)
    try:
        conn.request("HEAD", "/")
        conn.close()
        return True
    except socket.error:
        conn.close()
        return False


def check_network_connection():
    """
    Connect to the Tribute Kiosk Server to check internet connectivity
    :return:
    """
    url = URL_CONNECTIVITY.format(get_serial())
    try:
        r = requests.get(url, timeout=10, verify=False)
        if r.status_code == 200 and r.text.strip() == "Success":
            return True
        return False
    except Exception as e:
        print('Failed to connect to the server - {}'.format(e))
        return False


def get_current_ap():
    """
    Get currently connected AP's SSID
    :return:
    """
    if _is_rpi:
        pipe = os.popen('iwgetid -r')
        ap = pipe.read().strip()
        pipe.close()
        if ap == '':
            return None, None
        else:
            try:
                p = subprocess.Popen('iwconfig | grep "Access Point"', shell=True, stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                output, error = p.communicate()
                p.wait()
                line = output.decode('utf-8').splitlines()[-1].strip()
                mac = line.split('Access Point: ')[1].split()[0]
                return ap, mac
            except (ValueError, IndexError, ArithmeticError, TypeError):
                return None, None
    else:
        ap = 'Testing AP'
        mac = '11:22:33:44:55:66'
        return ap, mac


def get_current_interface():
    """
    Get currently connected wifi interface name
    :return:
    """
    if _is_rpi:
        pipe = os.popen('iwgetid')
        interface = pipe.read().split()[0]
        pipe.close()
    else:
        interface = 'wlan0'
        # ap = None
    return interface


def get_ip_address(ifname='wlan0', show_error=False):
    """
    Get assigned IP address of given interface
    :param show_error:
    :param ifname: interface name such as wlan0, eth0, etc
    :return: If not on RPi, returns the IP address of LAN
    """
    if not _is_rpi:
        ifname = 'enp3s0'
    try:
        return netifaces.ifaddresses(ifname)[netifaces.AF_INET][0]['addr']
    except Exception as e:
        Logger.error('Net: Failed to get IP address of {}, reason: {}'.format(ifname, e))


def get_ap_list(interface='wlan0'):
    ap_list = []
    if _is_rpi:
        try:
            l_cell = wifi.Cell.all(interface)
        except wifi.exceptions.InterfaceError:
            return []

        for cell in l_cell:
            str_q = cell.quality.split('/')     # quality: '43/70' or '68/70' or so
            quality = int(float(str_q[0]) / float(str_q[1]) * 100)  # We need percentage value
            ap_list.append({
                'ssid': cell.ssid,
                'signal': cell.signal,
                'quality': quality,
                'mode': cell.mode,
                'encrypted': cell.encrypted,
                'address': cell.address,
            })
        return ap_list
    else:
        name_list = ['Dummy ssid {}'.format(i) for i in range(30)]
        name_list += ['Testing AP', 'BuildingLink', 'A Long SSID, long, long, long, long, long, long ...']
        for ap_name in name_list:
            ap_list.append({
                'ssid': ap_name,
                'signal': random.randint(0, 100),
                'quality': random.randint(0, 100),
                'mode': '',
                'encrypted': random.choice([True, False]),
                'address': '11:22:33:44:55:66',
            })
        return ap_list


def get_wifi_strength():
    """
    Get wireless signal strength
    :return: Percentage value
    """
    if _is_rpi:
        pipe = os.popen('cat /proc/net/wireless')
        data = pipe.read().strip().splitlines()
        pipe.close()
        try:
            strength = data[2].split()[2][:-1]
            float(strength)
        except (IndexError, ValueError):
            # print('Failed to get current wifi strength, setting it to 0')
            strength = 0
        return round(float(strength) / 70.0 * 100, 2)
    else:
        return random.randint(0, 100)


def connect_to_ap(ssid='', pwd=None):
    """
    Connect to AP and return new assigned IP address
    :param ssid:
    :param pwd:
    :return: Return None when failed, otherwise return (IP, is_restored)
    """
    # Disconnect from the current AP:
    ap, mac = get_current_ap()
    if ap:
        print('Disconnecting {}...'.format(ap))
        p = subprocess.Popen('nmcli d disconnect wlan0', shell=True, preexec_fn=os.setpgrp,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        print('Result => Out: {}, Error: {}'.format(stdout, stderr))
        p.wait()

    time.sleep(.5)

    cmd = 'sudo nmcli dev wifi connect "{}" {}'.format(ssid, 'password "{}"'.format(pwd) if pwd is not None else '')
    Logger.info('Net: Connecting to {} - `{}`...'.format(ssid, cmd))
    p = subprocess.Popen(cmd, shell=True, preexec_fn=os.setpgrp, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    Logger.info('Net: Result => Out: {}, Error: {}'.format(stdout, stderr))
    p.wait()

    return get_ip_address('wlan0', show_error=False)


def get_saved_aps():
    """
    Read `hotspots.json` to get saved ssid/passwords
    :return:
    """
    json_data = {}
    try:
        json_data = json.loads(open(os.path.join(os.path.dirname(__file__), 'hotspots.json')).read())
    except Exception as e:
        print('Failed to get saved WiFi HotSpot info: {}'.format(e))
    return json_data


def update_ap_on_file(ssid, pwd):
    """
    Open the original `hotspots.json` and update
    :param ssid:
    :param pwd:
    :return:
    """
    json_data = get_saved_aps()
    json_data[ssid] = pwd
    with open(os.path.join(os.path.dirname(__file__), 'hotspots.json'), 'w+') as outfile:
        json.dump(json_data, outfile, ensure_ascii=False)


def forget_ap_on_file(ssid):
    json_data = get_saved_aps()
    if ssid in json_data.keys():
        del json_data[ssid]
        with open(os.path.join(os.path.dirname(__file__), 'hotspots.json'), 'w+') as outfile:
            json.dump(json_data, outfile, ensure_ascii=False)


def get_detail_of_connected_net(interface='wlan0'):
    """
    Get detail of connected network interface
    :param interface: `wlan0` in default
    :return:
    """
    ap_name, mac = get_current_ap()
    if ap_name is not None:
        strength = get_wifi_strength()
        ip = get_ip_address(interface)
        mac_addr = get_mac_address(interface)
        return {
            'ssid': ap_name,
            'quality': strength,
            'ip': ip,
            'mac': mac_addr
        }


def get_mac_address(interface='wlan0'):
    try:
        mac_str = open('/sys/class/net/' + interface + '/address').read()
    except Exception as e:
        Logger.error('Net: Failed to get MAC address of {}: {}'.format(interface, e))
        mac_str = "00:00:00:00:00:00"
    return mac_str[0:17]


def get_eth_settings():
    """
    Retrieve current ethernet settings
    """
    try:
        ip4 = netifaces.ifaddresses('eth0')[netifaces.AF_INET][0]
    except Exception as e:
        Logger.error('Net: Failed to get info of ethernet interface: {}'.format(e))
        ip4 = {'addr': '0.0.0.0', 'broadcast': '0.0.0.0', 'netmask': '255.255.255.0'}
    ip4['gateway'] = get_gateway('eth0')
    ip4['network'] = get_router_address('eth0')
    ip4['mode'] = get_network_mode('eth0')
    return ip4


def set_eth_settings(mode='dhcp', ip='192.168.1.110', netmask='255.255.255.0', gateway='192.168.1.254',
                     broadcast='192.168.1.255', network='192.168.1.0', backup=True):
    """
    Change ethernet settings
    :param backup:
    :param netmask: Subnet Mask Address
    :param network: Router IP
    :param broadcast: Broadcast IP Range
    :param gateway:
    :param mode: `dhcp` or `static`
    :param ip: IP address to be set
    :return: Return None when failed, otherwise return (IP, is_restored)
    """
    timestamp = int(time.time())
    if _is_rpi:
        file_name = '/etc/network/interfaces'
    else:
        file_name = os.path.dirname(os.path.realpath(__file__)) + '/test_net.txt'

    # Backup config file
    if backup:
        shutil.copyfile(file_name, '{}.backup.{}'.format(file_name, timestamp))
        wpa_file = '/etc/wpa_supplicant/wpa_supplicant.conf'
        shutil.copyfile(wpa_file, '{}.backup.{}'.format(wpa_file, timestamp))

    f = open(file_name, 'r+')
    f_content = f.readlines()
    line_num = -1
    for i in range(len(f_content)):
        if {'iface', 'inet', 'eth0'}.issubset(f_content[i].split()):
            line_num = i
            break
    if line_num >= 0:
        f_content[line_num] = 'iface eth0 inet {}\n'.format(mode)
    else:
        f_content.append('iface eth0 inet {}\n'.format(mode))
        line_num = len(f_content)

    data = {'address': ip, 'netmask': netmask, 'gateway': gateway, 'broadcast': broadcast, 'network': network}
    i = 1
    while mode != 'dhcp':
        try:
            _key = f_content[line_num + i].strip()
            try:
                _key = _key.split()[0]
            except IndexError:
                _key = ''
            if _key in data.keys():
                f_content[line_num + i] = '    ' + _key + ' ' + data.pop(_key) + '\n'
            else:
                _key = list(data.keys())[0]
                f_content.insert(line_num + 1, '    ' + _key + ' ' + data.pop(_key) + '\n')
        except IndexError:
            _key = list(data.keys())[0]
            f_content.append('    ' + _key + ' ' + data.pop(_key) + '\n')

        if len(data.keys()) == 0:
            break
        else:
            i += 1

    f.seek(0)
    f.truncate()
    f.write(''.join(f_content))
    f.close()
    return get_ip_address('eth0')


def restart_ethernet(interface='eth0'):
    if _is_rpi:
        print('Restarting {}...'.format(interface))
        p = subprocess.Popen(['ip link set {} down'.format(interface)], shell=True, close_fds=True)
        p.communicate()
        p.wait()
        p = subprocess.Popen(['ip link set {} up'.format(interface)], shell=True, close_fds=True)
        p.communicate()
        p.wait()
        time.sleep(3)
        # Wait for 10 sec until the network finishes restarting
        s_time = time.time()
        while True:
            ip = get_ip_address(interface, show_error=False)
            if ip:
                return ip
            else:
                if time.time() - s_time < 10:
                    time.sleep(.3)
                else:
                    return
    else:
        time.sleep(3)
        return get_ip_address(interface)


def get_gateway(interface='wlan0'):
    """
    Get gateway address
    """
    try:
        gateways = netifaces.gateways()[netifaces.AF_INET]
        gateway = [g[0] for g in gateways if g[1] == interface and g[2]][0]
    except IndexError:
        gateway = '0.0.0.0'
    return gateway


def get_router_address(interface='wlan0'):
    """
    Get router address of a interface
    """
    pipe = os.popen("netstat -nr")
    data = pipe.read().strip().splitlines()
    router_addr = '0.0.0.0'
    for r in data:
        sp = r.split()
        if sp[-1] == interface and sp[3] == 'U':
            router_addr = sp[0]
            break
    return router_addr


def get_network_mode(interface='wlan0'):
    """
    Get current networking mode
    :return: `dhcp`, `static`, `manual`
    """
    if _is_rpi:
        f = open('/etc/network/interfaces', 'r+')
        f_content = f.readlines()
        f.close()
        mode = 'dhcp'
        for line in f_content:
            if {'iface', interface, 'inet'}.issubset(line.split()):
                mode = line.split()[-1]
                break
        return mode.upper()
    else:
        return 'DHCP'


def get_external_ip():
    """
    Get external IP address of this machine
    """
    try:
        ip = get('https://api.ipify.org').text
        return ip
    except Exception as e:
        Logger.error('Net: Failed to get external IP, seems like no-internet? - {}'.format(e))


def get_geo_location():
    """
    Retrieve geo-location from http://ipinfo.io/json
    """
    url = 'http://ipinfo.io/json'
    try:
        response = urllib.request.urlopen(url)
        data = json.loads(response.read().decode('utf-8'))
        return data
    except Exception as e:
        Logger.error('Net: Failed to get geolocation, seems like no-internet? - {}'.format(e))


def get_lat_loc():
    data = get_geo_location()
    try:
        val = str(data['loc']).split(',')
        return val
    except (ValueError, AttributeError, TypeError):
        pass


def get_traffic():
    """
    Get the upload/download speed
    """
    global measure_time
    global traffic_data
    if traffic_data is None:
        traffic_data = psutil.net_io_counters(pernic=False)
        measure_time = time.time()
        return

    duration = time.time() - measure_time
    new_data = psutil.net_io_counters(pernic=False)
    try:
        upload = (new_data.bytes_sent - traffic_data.bytes_sent) / duration
        download = (new_data.bytes_recv - traffic_data.bytes_recv) / duration
        traffic_data = new_data
        measure_time = time.time()
        return {'upload': convert_to_bps(upload), 'download': convert_to_bps(download)}
    except Exception as e:
        Logger.error('Net: Failed to get network speed: {}'.format(e))


def convert_to_bps(speed):
    """
    Convert numeric speed value to bps expression.
    :param speed:
    :return:
    """
    if speed > 1024 * 1024 / 10:
        return '{} MB/s'.format(round(speed / 1024 / 1024, 1))
    elif speed > 1024 / 10:
        return '{} KB/s'.format(round(speed / 1024, 1))
    else:
        return '{} B/s'.format(round(speed))


if __name__ == '__main__':
    # set_eth_settings('static')
    print(get_geo_location())
