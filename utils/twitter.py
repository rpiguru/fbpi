# -*- coding: iso8859-15 -*-

import time
from kivy.logger import Logger
from pymongo import MongoClient
from twython import Twython
from settings import TWITTER_APP_KEY, TWITTER_APP_SECRET, TWITTER_OAUTH_TOKEN, TWITTER_OAUTH_SECRET, \
    MONGO_COLLECTION_TWITTER, MONGO_DB


def post_image_to_twitter(img_path, status='@nicole_kiosk My Tweet Status',
                          app_key=TWITTER_APP_KEY, app_secret=TWITTER_APP_SECRET,
                          oauth_token=TWITTER_OAUTH_TOKEN, oauth_token_secret=TWITTER_OAUTH_SECRET):
    """
    :param oauth_token_secret:
    :param oauth_token:
    :param app_secret:
    :param app_key:
    :param img_path:
    :param status:
    :return:
    """
    mongo_client = MongoClient('localhost')
    mongo_db = mongo_client[MONGO_DB]
    col_insert = mongo_db[MONGO_COLLECTION_TWITTER]

    data = {
        'image_path': img_path,
        'status': status,
        'app_key': app_key,
        'app_secret': app_secret,
        'oauth_token': oauth_token,
        'oauth_token_secret': oauth_token_secret,
        'time': time.time(),
        'sent': False
    }
    col_insert.insert_one(data)


def twitter_service():
    mongo_client = MongoClient('localhost')
    mongo_db = mongo_client[MONGO_DB]
    col_pop = mongo_db[MONGO_COLLECTION_TWITTER]

    while True:
        doc_list = list(col_pop.find({"sent": False}))
        if doc_list:
            for doc in doc_list:
                try:
                    twitter = Twython(app_key=doc['app_key'],
                                      app_secret=doc['app_secret'],
                                      oauth_token=doc['oauth_token'],
                                      oauth_token_secret=doc['oauth_token_secret'])
                    file = open(doc['image_path'], 'rb')
                    img_response = twitter.upload_media(media=file)
                    state_response = twitter.update_status(status=doc['status'], media_ids=[img_response['media_id']])
                    Logger.info('Twitter: Posted an image, state - '.format(state_response))
                    doc['sent'] = True
                    doc['sent_time'] = time.time()
                    col_pop.update_one({"_id": doc['_id']}, {"$set": {"sent": True}})
                except Exception as e:
                    Logger.error('Failed to post image to Twitter - {}'.format(e))
                    time.sleep(60)
        time.sleep(5)


if __name__ == '__main__':
    import multiprocessing

    multiprocessing.Process(target=twitter_service).start()

    post_image_to_twitter('../assets/images/sample.jpg')

    while True:
        time.sleep(1)
