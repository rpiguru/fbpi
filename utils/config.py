# -*- coding: iso8859-15 -*-

"""
    Utility for configuration
"""
import os
from multiprocessing import Process
import time
import xmltodict
import requests
import urllib
import hashlib
from settings import URL_CONNECTIVITY, URL_TEMPLATE, REMOTE_CHECK_INTERVAL, EVENT_PREPARE_TIME, CONFIG_FILE
from utils.common import get_serial
from kivy.logger import Logger

_cur_dir = os.path.dirname(os.path.realpath(__file__))

# import configparser
# ini_config = configparser.RawConfigParser()
# ini_config_file = os.path.expanduser('~/.kiosk.ini')

xml_config_file = os.path.join(_cur_dir, os.pardir, CONFIG_FILE)
sn = get_serial()


# def get_config(section, option, default_value=None):
#     """
#     Get configuration value
#     :param section:
#     :param option:
#     :param default_value:
#     :return:
#     """
#     ini_config.read(ini_config_file)
#     try:
#         val = ini_config.get(section=section, option=option)
#         if type(val) == list:
#             val = val[0]
#         return val
#     except configparser.NoSectionError:
#         return default_value
#     except configparser.NoOptionError:
#         return default_value
#
#
# def set_config(section, option, value):
#     if type(value) != str:
#         value = str(value)
#
#     ini_config.read(ini_config_file)
#     if section not in ini_config.sections():
#         ini_config.add_section(section=section)
#     ini_config.set(section, option, value)
#     try:
#         with open(ini_config_file, 'w') as configfile:
#             ini_config.write(configfile)
#         return True
#     except IOError as e:
#         # print('Failed to write default geo-location to file: {}'.format(e))
#         return False


def get_config_data():
    cnt = 0
    while True:
        try:
            data = xmltodict.parse(open(xml_config_file, 'r').read())
            break
        except Exception as e:
            Logger.error('Config: Failed to parse {} - {}'.format(xml_config_file, e))
            if cnt < 5:
                cnt += 1
                time.sleep(.001)
            else:
                raise ValueError('Error, {} is damaged!'.format(xml_config_file))
    return data


def get_event_by_name(event_name):
    event_data = get_config_data()
    events = event_data['eventconfig']['event']
    if type(events) != list:
        events = [events]
    for event in events:
        if event['@id'] == event_name:
            return event


class KioskConfigManager(Process):

    def __init__(self):
        super(KioskConfigManager, self).__init__()
        self.daemon = True

    def run(self):
        print("Sleeping for {} seconds in the network checking thread".format(EVENT_PREPARE_TIME))
        time.sleep(EVENT_PREPARE_TIME)

        while True:
            try:
                url = URL_CONNECTIVITY.format(sn)
                r = requests.get(url, timeout=10, verify=False)
                if r.status_code == 200 and r.text.strip() == "Success":
                    Logger.debug("Kiosk Config: Connected to the server - {}".format(url))
                    url = URL_TEMPLATE.format(sn)
                    Logger.debug("Kiosk Config: Now trying to download template from {}".format(url))
                    r = requests.get(url, timeout=60, verify=False)
                    if r.status_code == 200:
                        Logger.debug("Kiosk Config: Template downloaded, trying to parse it ...")
                        downloaded_xml = xmltodict.parse(r.text)
                        if downloaded_xml != get_config_data():
                            if pull_event_assets(downloaded_xml):
                                Logger.info('Kiosk Config: New config and its assets are downloaded, saving to file...')
                                with open(xml_config_file, "w") as configfile:
                                    configfile.write(r.text)
                        else:
                            Logger.info("Kiosk Config: Config is not changed.")
                    else:
                        Logger.error(
                            "Kiosk Config: Failed to download templates, status code: {}".format(r.status_code))
                else:
                    Logger.error("Kiosk Config: Failed to connect to the server - {}, Internet if off now?".format(url))
            except Exception as e:
                Logger.error('Kiosk Config: An Error occurred while playing with the server: {}'.format(e))

            print("Finished template handling, sleeping for {}s".format(REMOTE_CHECK_INTERVAL))
            time.sleep(REMOTE_CHECK_INTERVAL)


def pull_event_assets(data):
    Logger.debug('Kiosk Config: Pulling all assets from the server now...')
    try:
        events = data['eventconfig']['event']
        if type(events) != list:
            events = [events]
        for event in events:
            name = event['@id']
            base_path = "events/{}".format(name)
            if not os.path.exists(base_path):
                os.makedirs(base_path)
            template_overlay = event['templateoverlay']

            Logger.debug("Kiosk Config: Downloading {} into {}".format(template_overlay, base_path + "/overlay.png"))
            urllib.request.urlretrieve(template_overlay, base_path + "/overlay.png")

            if event['templateoverlayMD5'] == hashlib.md5(open(base_path + "/overlay.png", 'rb').read()).hexdigest():
                Logger.debug("Kiosk Config: Downloaded and Md5 verification passed")
            else:
                Logger.error("Kiosk Config: Hash validation failed")
                return False

            template_background = event['templatebackground']
            Logger.debug("Kiosk Config: Downloading {} into {}".format(template_background, base_path + "/default.jpg"))
            urllib.request.urlretrieve(template_background, base_path + "/default.jpg")

            if event['templatebackgroundMD5'] == hashlib.md5(open(base_path + "/default.jpg", 'rb').read()).hexdigest():
                Logger.debug("Kiosk Config: Downloaded and Md5 verification passed")
            else:
                Logger.error("Kiosk Config: Hash validation failed")
                return False
        return True
    except Exception as e:
        Logger.error('Kiosk Config: Failed to download assets - {}'.format(e))
        return False


if __name__ == '__main__':

    con = KioskConfigManager()
    con.start()

    while True:
        time.sleep(1)
