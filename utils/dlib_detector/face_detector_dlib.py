import copy
import glob
import cv2
import dlib


dlib_detector = dlib.get_frontal_face_detector()

dlib_up_sample = 1


def detect_face(frame):
    """
    Detect faces in the current frame
    :param frame:
    :return:
    """
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    face_rects = dlib_detector(gray_frame, dlib_up_sample)
    tmp_frame = copy.deepcopy(frame)
    rects = []
    if len(face_rects) > 0:
        for k, r in enumerate(face_rects):
            if r.left() < 0 or r.right() < 0 or r.top() < 0 or r.bottom() < 0:
                continue
            cv2.rectangle(tmp_frame, (r.left(), r.top()), (r.right(), r.bottom()), (0, 255, 0), 1)
            # print('{}:    Face found, ({}, {}) - ({}, {}),  {}x{}'.format(
            #     datetime.datetime.now(),
            #     r.left(), r.top(), r.right(), r.bottom(), r.right() - r.left(), r.bottom() - r.top()))
            rects.append(r)
    rects.sort(key=lambda f: abs(f.right() - f.left()) * abs(f.bottom() - f.top()), reverse=True)
    if rects:
        return rects[0]     # Use the largest one only.


def get_extended_face(frame, rect):
    # Get extended face rectangle
    factor = .1  # horizontal factor

    img_height, img_width, channels = frame.shape

    height = abs(rect.right() - rect.left())
    width = abs(rect.bottom() - rect.top())
    top = max(rect.top() - height * factor * 4, 0)
    bottom = min(rect.bottom(), img_height)
    left = max(rect.left() - width * factor, 0)
    right = min(rect.right() + width * factor, img_width)
    return frame[int(top):int(bottom), int(left):int(right)]


if __name__ == '__main__':

    import time
    for img in glob.glob('../model/sample/*.jpg'):
        s_time = time.time()
        faces = detect_face(cv2.imread(img))
        print('{}: {}, Elapsed: {}'.format(img, faces, time.time() - s_time))
