# -*- coding: iso8859-15 -*-

import threading
import time
import serial
from utils.common import is_rpi
from kivy.logger import Logger


class Relay:
    """This is an object used to talk with a USB relay bank.

    It provides a function to send commands and a function
    to output the serial connection settings.
    """

    def __init__(self):
        self._states = [0, 1, 'on', 'off']
        if is_rpi():
            try:
                self._ser = serial.Serial(port="/dev/ttyUSB0", baudrate=9600, parity=serial.PARITY_NONE, timeout=3,
                                          stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, xonxoff=True)
            except Exception as e:
                Logger.error('USB Relay: Failed to connect to the USB relay - '.format(e))

    def set(self, state=None):
        """
            Sets the state of a given channel.
            UserWarning
              -- If the state is not in [0, 1, 'on', 'off'].
        :param state: Set state of channel to on or off. Must be in [0, 1, 'on', 'off'].
        :type state: Boolean or String
        :return:
        """
        states = self._states
        if state not in states:
            msg = 'The state must be one of {0}'.format(states)
            raise UserWarning(msg, state)
        if state == 'on' or state == 1:
            state = 1
        if state == 'off' or state == 0:
            state = 0
        packet = [255, 1, state]
        Logger.debug('USB Relay: Turning relay {}'.format(state))
        if is_rpi() and hasattr(self, '_ser'):
            self._ser.write(packet)


def turn_relay(state='off'):
    threading.Thread(target=_turn_relay, args=(state, )).start()


def _turn_relay(state):
    relay_ctrl = Relay()
    relay_ctrl.set(state)


if __name__ == '__main__':
    _relay_ctrl = Relay()
    cmd_list = []

    for cmd in cmd_list:
        _relay_ctrl.set(cmd)
        time.sleep(0.2)
