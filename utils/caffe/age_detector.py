import glob
import os
import sys
import cv2
import numpy as np

if 'arm' in os.uname()[4]:  # Is RPi?
    sys.path.append('/home/pi/caffe-rc5/python')

import caffe

model_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'model')

# Loading the mean image
mean_filename = os.path.join(model_dir, 'mean.binaryproto')
proto_data = open(mean_filename, "rb").read()
a = caffe.io.caffe_pb2.BlobProto.FromString(proto_data)
mean = caffe.io.blobproto_to_array(a)[0]

# Loading the age network
age_net_pretrained = os.path.join(model_dir, 'age_net.caffemodel')
age_net_model_file = os.path.join(model_dir, 'deploy_age.prototxt')
age_net = caffe.Classifier(age_net_model_file, age_net_pretrained,
                           mean=mean,
                           channel_swap=(2, 1, 0),
                           raw_scale=255,
                           image_dims=(256, 256))

# Labels
age_list = ['(0, 2)', '(4, 6)', '(8, 12)', '(15, 20)', '(25, 32)', '(38, 43)', '(48, 53)', '(60, 100)']


def detect_age(img):
    """
    Detect age from a numpy array image
    :param img: numpy array image
    :return:
    """
    face = cv2.resize(img, (256, 256))

    # Convert ndarray type form int to float32 (0.0 ~ 1.0)
    face = face.astype(np.float32) / 255.0

    # Age prediction
    prediction = age_net.predict([face])
    index = prediction[0].argmax()
    if index == 0:  # Shouldn't be (0, 2), so ignore this and get the max value again.
        index = prediction[0][1:].argmax() + 1
    return age_list[index], round(prediction[0][index] * 100, 4)


if __name__ == '__main__':

    import time

    for img_path in glob.glob('../model/sample/*.jpg'):
        s_time = time.time()
        image = cv2.imread(img_path)
        age = detect_age(image)
        print('{}: {}, elapsed: {}'.format(img_path, age, time.time() - s_time))
