# -*- coding: iso8859-15 -*-

import glob
import os
import random
from PIL import Image
from kivy.logger import Logger
import subprocess
import time
from moviepy.editor import VideoFileClip
from settings import IMAGE_LAPSED, HEIGHT, PICAMERA_RECORD_WIDTH, PICAMERA_RECORD_HEIGHT
from utils.common import get_serial
from utils.config import get_config_data


def create_motion_video(src_file, remove_src=True):
    """
    Create a custom slow-motion video with 5-sec h264 video in 90 fps.
    First 2 seconds in normal and the next 2 seconds in slow and last 1 seconds in normal again.
    Total length would be 3 (7 sec of slow-mo converted to normal) + (9sec of slow-mo) + (5 sec of slow-mo) = 21 sec
    :param remove_src:
    :param src_file: Full path of .h264 file
    :return:
    """
    Logger.debug('Motion Video: Creating slow-motion video from {}'.format(src_file))
    s_time = time.time()
    timestamp = time.time().__int__()
    audio_num = random.randint(1, 6)
    # commands = [
    #     'MP4Box -fps 30 -add {} /tmp/{}.mp4'.format(src_file, timestamp),
    #     # Cut partial video(0~6 sec) with 3x speed
    #     'ffmpeg -t 6 -i /tmp/{}.mp4 -vf "setpts=(1/3)*PTS" /tmp/part-1.mp4 -y'.format(timestamp),
    #     # trans-code video to mpeg stream
    #     'ffmpeg -i /tmp/part-1.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts /tmp/part-1.ts -y',
    #     # Cut partial video(6~15 sec) with 3x speed
    #     'ffmpeg -ss 6 -i /tmp/{}.mp4 -t 9 -c copy /tmp/part-2.mp4 -y'.format(timestamp),
    #     # trans-code video to mpeg stream
    #     'ffmpeg -i /tmp/part-2.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts /tmp/part-2.ts -y',
    #     # Cut video(15~) with 3x speed
    #     'ffmpeg -ss 15 -i /tmp/{}.mp4 -t 6 -vf "setpts=(1/3)*PTS" /tmp/part-3.mp4 -y'.format(timestamp),
    #     'ffmpeg -i /tmp/part-3.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts /tmp/part-3.ts -y',
    #     # Concat
    #     'ffmpeg -i "concat:/tmp/part-1.ts|/tmp/part-2.ts|/tmp/part-3.ts" '
    #     '-i assets/sounds/action{}.mp3 -c copy {}.mp4 -y'.format(audio_num, src_file[:-5]),
    # ]
    commands = [
        'MP4Box -fps 30 -add {} /tmp/{}.mp4'.format(src_file, timestamp),
        # Cut partial video(0~6 sec) with 3x speed
        'mencoder -endpos 00:00:02 -speed 3 -o /tmp/part-1.mp4 -ovc copy /tmp/{}.mp4'.format(timestamp),
        # trans-code video to mpeg stream
        'ffmpeg -i /tmp/part-1.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts /tmp/part-1.ts -y',
        # Cut partial video(6~15 sec) with 3x speed
        'ffmpeg -ss 6 -i /tmp/{}.mp4 -t 9 -c copy /tmp/part-2.mp4 -y'.format(timestamp),
        # trans-code video to mpeg stream
        'ffmpeg -i /tmp/part-2.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts /tmp/part-2.ts -y',
        # Cut video(15~) with 3x speed
        'mencoder -ss 00:00:15 -speed 3 -o /tmp/part-3.mp4 -ovc copy /tmp/{}.mp4'.format(timestamp),
        # trans-code video to mpeg stream
        'ffmpeg -i /tmp/part-3.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts /tmp/part-3.ts -y',
        # Concat
        'ffmpeg -i "concat:/tmp/part-1.ts|/tmp/part-2.ts|/tmp/part-3.ts" '
        '-i assets/sounds/action{}.mp3 -c copy {}.mp4 -y'.format(audio_num, src_file[:-5]),
    ]

    for cmd in commands:
        print(cmd)
        start_time = time.time()
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.communicate()
        p.wait()
        print('Elapsed: ', time.time() - start_time)

    tmp_files = ['/tmp/{}.mp4'.format(timestamp)]
    if remove_src:
        tmp_files.append(src_file)
    for f in tmp_files:
        try:
            os.remove(f)
        except FileNotFoundError:
            pass
    result_file = src_file.replace('h264', 'mp4')
    Logger.info('Motion Video: ===== Created: {}, Elapsed: {}'.format(result_file, time.time() - s_time))
    return result_file


def get_video_length(video_file):
    clip = VideoFileClip(video_file)
    return clip.duration


def get_video_size(video_file):
    """
    Get resolution of a video file.
    :param video_file:
    :return:
    """
    # clip = VideoFileClip(video_file)
    # return clip.size
    # For now, use constant value as it takes about 1~2 sec on RPi... :(
    return [1280, 720]


def compose_time_lapsed_video():
    """
    Create time lapse video of finished events.
    :return:
    """
    xml_events = get_config_data()['eventconfig']['event']
    if type(xml_events) != list:
        xml_events = [xml_events]
    for event_id in [d for d in os.listdir('events') if os.path.isdir(d) and d != 'default']:
        if event_id in [e['@id'] for e in xml_events]:
            xml_event = [e for e in xml_events if e['@id'] == event_id][0]
            if int(xml_event['EndTime']) > time.time():
                Logger.info('Time-lapse: {} is still in progress, ignoring...'.format(event_id))
                continue

        target = 'events/lapsed-{}-{}.mp4'.format(get_serial(), int(time.time()))
        base_dir = os.path.join('events', event_id, IMAGE_LAPSED)
        for path in glob.glob('{}/*.jpg'.format(base_dir)):
            img = Image.open(path)
            if img.size[0] != PICAMERA_RECORD_WIDTH:
                resized_img = img.resize((PICAMERA_RECORD_WIDTH, PICAMERA_RECORD_HEIGHT), Image.ANTIALIAS)
                resized_img.save(path)
        txt_file = '/tmp/images.txt'
        with open(txt_file, 'w') as f:
            for path in glob.glob('{}/*.jpg'.format(base_dir)):
                f.write("file '{}'".format(path))
        cmd = 'ffmpeg -y -f concat -r 1/15 -safe 0 -i "{}" -c:v libx264 -vf "fps=30,format=yuv420p" "{}"'.format(
            txt_file, target)
        Logger.info('Time-lapsed Video: {}'.format(cmd))
        p = subprocess.Popen(cmd, shell=True, preexec_fn=os.setpgrp, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.communicate()
        # print('Result => Out: {}, Error: {}'.format(stdout, stderr))
        p.wait()
        for img in glob.glob(base_dir + '/*.jpg'):
            os.remove(img)
        if os.path.exists(target):
            Logger.info('Time-lapsed Video: Created video - {}({})'.format(target, os.path.getsize(target)))


def play_video_on_widget(widget=None, size=None, pos=None, video_src=None):
    if widget:
        size = widget.size
        pos = widget.pos
    x1 = int(pos[0])
    x2 = int(x1 + size[0])
    y1 = int(HEIGHT - pos[1] - size[1])
    y2 = int(y1 + size[1])
    cmd = "omxplayer -o local --aspect-mode letterbox --layer 20 --display 5 --win {},{},{},{} " \
          "{}".format(x1, y1, x2, y2, video_src)
    Logger.debug('Nicole: {}'.format(cmd))
    subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


if __name__ == '__main__':

    _s_time = time.time()
    result = create_motion_video('~/Videos/1.h264', remove_src=False)
    print('Elapsed: ', time.time() - _s_time, ' Result: {}'.format(result))
    # compose_time_lapsed_video()
