# -*- coding: iso8859-15 -*-

import cups
from kivy.logger import Logger
from utils.common import is_rpi


def print_file(file_name, layout='4x6', copies=1):
    if is_rpi():
        try:
            conn = cups.Connection()
            printers = conn.getPrinters()
            printer_name = get_current_printer_name(printers)
            if printer_name:
                options = {'copies': str(copies), 'collate': 'true'}
                if layout == '2x6':
                    options.update({'PageSize': 'w288h432-div2'})
                result = conn.printFile(printer=printer_name, filename=file_name, title='Print label', options=options)
                Logger.debug('Printing `{}`: {} '.format(file_name, result))
                return result
            else:
                Logger.error('Printer: Cannot find any printer!')
        except Exception as e:
            Logger.error('Failed to print file({}): {}'.format(file_name, e))
    else:
        print('Printed {}'.format(file_name))


def get_current_printer_name(printers):
    """
    Return currently connected printer name
    :return:
    """
    for _key, _item in printers.items():
        if 'usb://' in str(_item['device-uri']):
            return _key


if __name__ == '__main__':
    pass
