#!/usr/bin/env python3
import os
import cv2
import dlib
import numpy as np
import tensorflow as tf
import time
from imutils.face_utils import FaceAligner
from utils.tf_detector.inception_resnet_v1 import inference


sess = tf.Session()

images_pl = tf.placeholder(tf.float32, shape=[None, 160, 160, 3], name='input_image')

images_norm = tf.map_fn(lambda frame: tf.image.per_image_standardization(frame), images_pl)

train_mode = tf.placeholder(tf.bool)

age_logits, gender_logits, _ = inference(images_norm, keep_probability=0.8, phase_train=train_mode, weight_decay=1e-5)

gender = tf.argmax(tf.nn.softmax(gender_logits), 1)

age_ = tf.cast(tf.constant([i for i in range(0, 101)]), tf.float32)
age = tf.reduce_sum(tf.multiply(tf.nn.softmax(age_logits), age_), axis=1)

init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())

sess.run(init_op)

saver = tf.train.Saver()

cur_dir = os.path.dirname(os.path.realpath(__file__))

ckpt = tf.train.get_checkpoint_state(os.path.join(cur_dir, 'models'))
if ckpt and ckpt.model_checkpoint_path:
    saver.restore(sess, ckpt.model_checkpoint_path)
    print("Model is restored!")


detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(os.path.join(cur_dir, os.pardir, "dlib_detector", "models",
                                              "shape_predictor_68_face_landmarks.dat"))
fa = FaceAligner(predictor, desiredFaceWidth=160)


def detect_from_frame(frame):
    img_size = 160

    input_img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    img_h, img_w, _ = np.shape(input_img)

    # detect faces using dlib detector
    detected = list(detector(input_img, 1))
    detected.sort(key=lambda f: abs(f.right() - f.left()) * abs(f.bottom() - f.top()), reverse=True)

    faces = np.empty((len(detected), img_size, img_size, 3))

    for i, d in enumerate(detected):
        x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
        cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 0, 0), 2)
        # xw1 = max(int(x1 - 0.4 * w), 0)
        # yw1 = max(int(y1 - 0.4 * h), 0)
        # xw2 = min(int(x2 + 0.4 * w), img_w - 1)
        # yw2 = min(int(y2 + 0.4 * h), img_h - 1)
        # cv2.rectangle(img, (xw1, yw1), (xw2, yw2), (255, 0, 0), 2)
        faces[i, :, :, :] = fa.align(input_img, gray, detected[i])
        # faces[i,:,:,:] = cv2.resize(img[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))

    if len(detected) > 0:
        # predict age and gender of the detected faces
        ages, genders = sess.run([age, gender], feed_dict={images_pl: faces, train_mode: False})
    else:
        ages, genders = [], []
    ages = [int(a) for a in ages]
    genders = ["F" if g == 0 else "M" for g in genders]

    return detected, ages, genders


def detect_from_webcam():
    """
    Detect Age/Gender from the webcam
    :return:
    """

    def draw_label(image, point, _label, font=cv2.FONT_HERSHEY_SIMPLEX, font_scale=1, thickness=2):
        size = cv2.getTextSize(_label, font, font_scale, thickness)[0]
        x, y = point
        cv2.rectangle(image, (x, y - size[1]), (x + size[0], y), (255, 0, 0), cv2.FILLED)
        cv2.putText(image, _label, point, font, font_scale, (255, 255, 255), thickness)

    # capture video
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

    while True:
        ret, img = cap.read()
        if not ret:
            print("error: failed to capture image")
            return
        s_time = time.time()
        faces, ages, genders = detect_from_frame(img)
        # draw results
        for i, d in enumerate(faces):
            label = "{}, {}".format(ages[i], genders[i])
            draw_label(img, (d.left(), d.top()), label)

        if 'arm' not in os.uname()[4]:
            cv2.imshow("result", img)
            key = cv2.waitKey(1)
            if key == 27:
                break
        else:
            print('Age: {}, Gender: {}'.format(ages, genders))

        print('Elapsed: {}'.format(time.time() - s_time))


if __name__ == '__main__':
    # detect_from_webcam()
    import glob
    for _img in glob.glob('../../assets/images/sample/faces/*.jpg'):
        frame = cv2.imread(_img)
        print(_img, ':', detect_from_frame(frame))
