import time
import cv2
import os
from pymongo import MongoClient
from settings import MONGO_DB, MONGO_COLLECTION_FACE
from utils.caffe.age_detector import detect_age
from utils.dlib_detector.face_detector_dlib import detect_face, get_extended_face
from utils.caffe.gender_detector import detect_gender
import threading


mongo_client = MongoClient('localhost')
mongo_db = mongo_client[MONGO_DB]
col_face = mongo_db[MONGO_COLLECTION_FACE]


class AgeGenderDetector(threading.Thread):

    b_stop = threading.Event()

    def __init__(self):
        super(AgeGenderDetector, self).__init__()
        self.b_stop.clear()

    def run(self):
        while not self.b_stop.isSet():
            doc_list = list(col_face.find({"processed": False}))
            print(doc_list)
            if doc_list:
                for doc in doc_list:
                    print('Found an image - {}'.format(doc['image_path']))
                    frame = cv2.imread(doc['image_path'])
                    rect = detect_face(frame)
                    print('Detected Faces - {}'.format(rect))
                    if rect:
                        face = frame[rect.top():rect.bottom(), rect.left():rect.right()]
                        extended_face = get_extended_face(frame, rect)
                        s_time = time.time()
                        age, age_confidence = detect_age(extended_face)
                        if age in ['(0, 2)', '(4, 6)']:
                            print('Age/Gender: Detected age is wrong, maybe cropped too large? Retrying...')
                            age, age_confidence = detect_age(face)
                        print('===== Age: {}, confidence: {}%, elapsed: {} sec'.format(
                            age, age_confidence, time.time() - s_time))
                        s_time = time.time()
                        gender, gender_confidence = detect_gender(face)
                        print('===== Gender: {}, confidence: {}%, elapsed: {} sec'.format(
                            gender, gender_confidence, time.time() - s_time))
                        col_face.update_one({"_id": doc['_id']},
                                            {"$set": {
                                                "processed": True,
                                                "age": age,
                                                "gender": gender,
                                                "age_confidence": age_confidence,
                                                "gender_confidence": gender_confidence
                                            }})
                    else:
                        print('Age/Gender: Face not found - {}'.format(doc['image_path']))
                        col_face.update_one({"_id": doc['_id']}, {"$set": {"processed": True}})
                    # try:
                    #     os.remove(doc['image_path'])
                    # except Exception as e:
                    #     print('Age/Gender: Failed to delete {} - {}'.format(doc['image_path'], e))
            time.sleep(5)

    def stop(self):
        self.b_stop.set()


if __name__ == '__main__':

    detector = AgeGenderDetector()

    try:
        detector.start()
    except KeyboardInterrupt:
        detector.stop()
